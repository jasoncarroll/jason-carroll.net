var http = require('http'),
    fs = require('fs'),
    Browser = require("zombie"),
    assert = require("assert");

var instance,
    browser = new Browser( { debug: false } );

describe('Website Tests', function() {

//    before(function(done) {
//
////        instance = http.createServer(function (request, response) {
////
////            // Attach listener on end event.
////            request.on("end", function () {
////                // Read the file.
////                fs.readFile("index.html", function (err, html) {
////                    response.writeHead(200, {'Content-Type': 'text/html' });
////                    response.write(html);
////                    response.end();
////                });
////            });
////
////        // Listen on the 8000 port.
////        }).listen(8000);
//
//
//
////        browser.visit("http://localhost:8000/", function(e, browser) {
////            browser.on("error", function(error) {
////                console.error(error);
////            });
////            done();
////        });
//
//        done();
//    });
//
//    after(function(done){
//        //instance.close();
//        done();
//    });





    it("Should have my name as the title", function(done) {
        browser.visit("http://localhost:8000/index.html", function(e, browser) {
            assert.ok(browser.success);
            assert.equal(browser.text("title"), "Jason Carroll");
            done();
        });
    });


    it("Should init as the intro page", function(done) {

        browser.visit("http://localhost:8000/index.html", function() {
            assert.ok(browser.success);

            var heroHolderContent = browser.query('#hero-holder').innerHTML;
            var hasIntroText = (heroHolderContent.indexOf("Hi there") !== -1);

            assert.equal(hasIntroText, true);
            done();
        });
    });


    it("Portfolio loaded into hero-holder", function(done) {

        browser.visit("http://localhost:8000/index.html", function(){
            assert.ok(browser.success);

            var navPortfolio = browser.query('#navPortfolio');

            browser.fire("click", navPortfolio, function(){

                var portfolioHeader = browser.query('#hero-holder H1');

                assert.equal(portfolioHeader.innerHTML, 'Portfolio');
                done();
            });
        });
    });
});
