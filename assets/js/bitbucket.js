/**
 * Created with JetBrains WebStorm.
 * User: Jason
 * Date: 12/28/12
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */

var BitBucket = function () {
    this.profileUrl = "https://api.bitbucket.org/1.0/users/jasoncarroll/";
    this.RecentCheckins = null;
    this.RepositoryList = null;
};

BitBucket.prototype = function () {


    var profileUrl,
        GetRepositoryList = function () {
        if (this.RepositoryList === null) {
            this.RepositoryList = [];
            var myThis = this;

            $.ajax({
                url: this.ProfileURL,
                dataType: 'jsonp',
                success: function(data) {

                    $.each(data.repositories, function(i, k) {

                        myThis.RepositoryList.push(
                            {
                                "name" : k.name,
                                "resource_uri" : k.resource_uri,
                                "last_updated" : k.last_updated,
                                "recent_checkins" : []
                            }
                        );

                    });
                }
            });
        } else {

        }
        };


    return {
        GetRepositoryList : GetRepositoryList,
        ProfileURL: profileUrl
    };
}();
