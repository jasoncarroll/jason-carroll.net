
var AppContext = function () {
    this.CurrentView = "Home";
    this.RecentCheckins = null;
    this.RepositoryList = null;
};

AppContext.prototype = function () {

    var currentView,
        recentCheckins,
        repositoryList,
        unloadContent = function () {
            $('#hero-holder').html("");
            $('div.well').removeClass('pressed');
            $('#name-header').hide();
        },
        ShowHome = function() {
            this.CurrentView = "Home";
            unloadContent();
            $('#name-header').show();
            $('#home-hidden').clone().appendTo('#hero-holder').show();

            $('.navItem').removeClass('active');
            $('#navHome').addClass('active');
        },
        PortfolioClicked = function() {
            if (this.CurrentView === "Portfolio") {
                ShowHome.call(this);
            } else {
                showPortfolio.call(this);
            }
        },
        ResumeClicked = function() {
            if (this.CurrentView === "Resume") {
                ShowHome.call(this);
            } else {
                showResume.call(this);
            }
        },
        ContactClicked = function () {
            if (this.CurrentView === "Contact") {
                ShowHome.call(this);
            } else {
                showContact.call(this);
            }
        },
        showPortfolio = function () {
            this.CurrentView = "Portfolio";
            unloadContent();

            $('#portfolio-hidden').clone().appendTo('#hero-holder').show();
            $('div.portfolio.well').addClass('pressed');

            $('.navItem').removeClass('active');
            $('#navPortfolio').addClass('active');

            GetRepositoryList.call(this);
            GetRecentCheckins.call(this);

        },
        showResume = function () {
            this.CurrentView = "Resume";
            unloadContent();

            $('#resume-hidden').clone().appendTo('#hero-holder').show();
            $('div.resume.well').addClass('pressed');

            $('.navItem').removeClass('active');
            $('#navResume').addClass('active');
        },
        showContact = function() {
            this.CurrentView = "Contact";
            unloadContent();

            $('#contact-hidden').clone().appendTo('#hero-holder').show();
            $('div.contact.well').addClass('pressed');

            $('.navItem').removeClass('active');
            $('#navContact').addClass('active');
        },
        GetRepositoryList = function ()
        {
            if (this.RepositoryList === null) {
                this.RepositoryList = [];
                var myThis = this;

                $.ajax({
                    url: 'https://api.bitbucket.org/1.0/users/jasoncarroll/',
                    dataType: 'jsonp',
                    success: function(data) {

                        $.each(data.repositories, function(i, k) {

                            myThis.RepositoryList.push(
                                {
                                    "name" : k.name,
                                    "resource_uri" : k.resource_uri,
                                    "last_updated" : k.last_updated,
                                    "recent_checkins" : []
                                }
                            );

                        });
                        LoadRepoTemplates.call(myThis);
                    }
                });
            } else {
                LoadRepoTemplates.call(this);
            }
        },

        // This API call is going to take a while. TODO: Move to server-side/db store.
        GetCheckinList = function() {
            if (this.RecentCheckins === null) {
                this.RecentCheckins = [];
                var myThis = this;

                $.each(this.RepositoryList, function(i, k)
                {

                });
            }
        },
        GetCheckinsByRepoName = function ( name, resource_uri, limit)
        {
            $.ajax({
                url: 'https://api.bitbucket.org/' + resource_uri + 'changesets?limit=' + limit,
                dataType: 'jsonp',
                success: function(data) {

                    $.each(data.changesets, function(i, k) {

                        myThis.RecentCheckins.push(
                            {
                                "node" : k.node,
                                "timestamp" : k.timestamp,
                                "message" : k.message
                            }
                        );

                    });

                    LoadCheckinTemplates.call(myThis);
                }

            });
        },
        GetRecentCheckins = function() {
            if (this.RecentCheckins === null) {
                this.RecentCheckins = [];
                var myThis = this;

                $.ajax({
                    url: 'https://api.bitbucket.org/1.0/repositories/jasoncarroll/jason-carroll.net/changesets?limit=3',
                    dataType: 'jsonp',
                    success: function(data) {

                        $.each(data.changesets, function(i, k) {

                            myThis.RecentCheckins.push(
                                {
                                   "node" : k.node,
                                   "timestamp" : k.timestamp,
                                   "message" : k.message
                                }
                            );

                        });

                        LoadCheckinTemplates.call(myThis);
                    }
                });
            }
            else {
                LoadCheckinTemplates.call(this);
            }
        },
        LoadRepoTemplates = function () {
            var source   = $("#repo-template").html();
            var template = Handlebars.compile(source);
            var formattedData = '{ "RepositoryList" : ' + JSON.stringify(this.RepositoryList) + '}';
            $('#repo-container').html(template($.parseJSON(formattedData)));
        },
        LoadCheckinTemplates = function() {
            var source   = $("#checkin-template").html();
            var template = Handlebars.compile(source);
            var formattedData = '{ "RecentCheckins" : ' + JSON.stringify(this.RecentCheckins) + '}';
            $('#checkin-container').html(template($.parseJSON(formattedData)));
        };

        return {
            PortfolioClicked: PortfolioClicked,
            ResumeClicked: ResumeClicked,
            ContactClicked: ContactClicked,
            CurrentView: currentView,
            RecentCheckins: recentCheckins,
            RepositoryList: repositoryList,
            GetRepositoryList: GetRepositoryList,
            ShowHome: ShowHome,
            GetRecentCheckins: GetRecentCheckins
        };

}();



